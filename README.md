# Android Tests for Budget Watch

This project is a clone of [Budget Watch](https://github.com/brarcher/budget-watch), an open source Android app to manage personal budgets. The goal of this project is to quickly show the use of Espresso for the functional testing of an Android app, both on emulators and real devices.

These tests were developed by [Sara Raap-van Bussel](https://gitlab.com/centric-sararaap) for [Centric](https://www.centric.eu).

**Make sure to use the "espresso-tests" branch (the only branch present in this repository)**

# How to

## Set up Android Studio

* [Download Android Studio](https://developer.android.com/studio/index.html)
* [Install Android Studio](https://developer.android.com/studio/install.html)
* Open Android Studio

## Open project
* [Clone this repository] (https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html#clone-your-project)
* Open the cloned project in Android Studio
* Choose open and point to the folder of the cloned project
* The project will be imported based on the included Gradle files
* [Add a Test Configuration](https://google.github.io/android-testing-support-library/docs/espresso/setup/#running-tests)
* Tests can be found under App/Java/androidTest. [You can run a whole test file or individual tests](https://developer.android.com/studio/test/index.html#run_a_test).

## To test on a real devices

* [Enable developer mode on the device](https://developer.android.com/studio/run/device.html#developer-device-options)
* If on Windows: [Install USB driver] (https://developer.android.com/studio/run/oem-usb.html)
* Connect device and run test. Device should be selectable for test.

## To test on an emulator

* [Create an emulator](https://developer.android.com/studio/run/managing-avds.html)
* When running a test, select prefered emulator for executing the test.
